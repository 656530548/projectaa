

import argparse
import os
from glob import glob
from collections import Counter
import matplotlib.pyplot as plt


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', default='face_imgs/')
    parser.add_argument('--output_dir', default='/')
    parser.add_argument('--date', default='all')  # 单一日期：2021_07_09， 日期段：2021_07_29-2021_07_30， 全部：all
    return parser.parse_args()




def files_pids_imtypes(files):
    pids = []
    imtypes = []
    files = [i.split("/")[-1] for i in files]
    for file in files:
        name, ext = file.split('.')
        if ext == 'jpg':
            # pid2021071111251500543539_imtype2_11_28_48_code502_cmpres-1.jpg  # de

            # pid20210727_009_imtype2_20_05_03_code200_cmpres1_liveres1.jpg
            # pid2021071111251500543539_imtype2_11_28_48_code502_cmpres-1_liveres1.jpg
            # pid
            pid_start_idx = name.find('pid') + len('pid')
            pid_end_idx = name.find('imtype') - 1
            pid = file[pid_start_idx:pid_end_idx]
            pids = pids + [pid]
            # imtype


            imtype_idx = name.find('imtype')
            imtype = file[imtype_idx]
            imtypes = imtypes + [imtype]
    return pids, imtypes


def count_pid(files):
    pids, imtypes = files_pids_imtypes(files)
    # pids去重，构成pids_total
    pids_total = []
    [pids_total.append(i) for i in pids if i not in pids_total]
    print('Pids total: ' + str(len(pids_total)))

    # 去除imtypes为1和0的pid,构成pids_2
    pids_2 = []
    for ind, pid in enumerate(pids):
        if imtypes[ind] == '2':
            pids_2.append(pid)
    # print(len(pids_2))
    pids_2_count = dict(Counter(pids_2))
    pids_valid = [i for i in pids_2_count if pids_2_count[i] > 8]
    'Valid Pids total: '


def count_img(args, files):
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)
    pids, imtypes = files_pids_imtypes(files)
    img_count = dict(Counter(pids))
    img_count_sorted = sorted(img_count.items(), key=lambda img_count: img_count[1], reverse=True)

    with open(args.date + '_count_img_results.txt', 'w') as img_count_txt:
        for img in img_count_sorted:
            img_count_txt.write(img[0] + ': ' + str(img[1]) + '\n')
        # img_count_txt.write('\n')
    print('The number of pictures per PID has been successfully output as a TXT file with the path: ' + args.output_dir)


def img_pid_plot(files):
    pids, imtypes = files_pids_imtypes(files)
    img_count = dict(Counter(pids))
    img_count_sorted = sorted(img_count.items(), key=lambda img_count: img_count[1], reverse=True)
    # 每种图片数量的pid数量
    num = [i[1] for i in img_count_sorted]
    img_pid = dict(Counter(num))

    # 构建数据与绘图
    x_data = [i for i in img_pid]
    y_data = [img_pid[i] for i in img_pid]
    plt.bar(x=x_data, height=y_data, color='steelblue', alpha=0.8)
    for x, y in enumerate(y_data):
        plt.text(x, y + 100, '%s' % y, ha='center', va='bottom')
    plt.title('Number of PIDs corresponding to different number of pictures')
    plt.xlabel('Pictures number')
    plt.ylabel('PID number')
    plt.show()
    print('Drawing plot succeeded.')


def data_reader(args):
    date_dirs = glob(args.input_dir + '*/')
    dates = [dd.split("/")[-2] for dd in date_dirs]
    # print(dates)
    if args.date == 'all':
        date_dirs = glob(args.input_dir + '*/')
        files = []
        for date_dir in date_dirs:
            files_dir = glob(date_dir + '*/')
            for fd in files_dir:
                files = files + glob(fd + '*.jpg')
        print('Pictures total:' + str(len(files)))
        return files

    elif args.date in dates:
        # args.date.split('_')
        date_dirs = glob(args.input_dir + args.date + '/*/')
        files = []
        for file_dir in date_dirs:
            files = files + glob(file_dir + '*jpg')
        print('Pictures total:' + str(len(files)))
        return files

    else:
        date_range = args.date.split('-')
        files = []
        index_1 = dates.index(date_range[0])
        index_2 = dates.index(date_range[1])
        dates_range = dates[index_1:index_2 + 1]
        # print(dates_range)
        for date in dates_range:
            date_dirs = glob(args.input_dir + date + '/*/')
            for file_dir in date_dirs:
                files = files + glob(file_dir + '*jpg')
        print('Pictures total:' + str(len(files)))
        return files


def main(args):
    files = data_reader(args)
    count_pid(files)
    count_img(args, files)
    img_pid_plot(files)


if __name__ == '__main__':
    args = get_args()
    main(args)

